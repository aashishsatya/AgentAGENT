# AgentAGENT:

AgentAGENT is an Acronym GENeraTor written in C++. My primary use of the software is to give names to upcoming projects. The idea for the name AgentAGENT was got by running the program!

## Running the program:

Linux: Access the bin folder, compile the file AgentAGENT.cpp and run the resulting executable.

P. S. If you're here you might also be interested in [Acronimify](http://acronymify.com/) (disclaimer: the website is not mine).
