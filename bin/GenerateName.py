# -*- coding: utf-8 -*-
"""
Created on Sun Jul 15 10:28:14 2018

@author: aashishsatya
"""

import itertools
import os.path

def powerset(iterable):
    "powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    return itertools.chain.from_iterable(itertools.combinations(s, r) for r in range(len(s)+1))

letters = 'HELLO'
for word in powerset(list(letters)):
    if(len(word) < 2):
        continue
    file_name = word[0] + word[1] + '.txt'
    if os.path.exists(file_name):
        with open(file_name) as f:
            for file_word in f:
                print(file_word, end = '\n')
                if word == file_word:
                    print(word)