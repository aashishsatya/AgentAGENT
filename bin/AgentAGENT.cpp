#include <fstream>
#include <iostream>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cmath>

using namespace std;

// to store a word or string, so that it can be passed without
// worrying about mutation by a function
struct char_array
{
    char word[11];
};

// variables

ifstream reqd_dict_file;   //to accept input from the corresponding dictionary file
fstream found_words_file;  //to check if the word has already been found

char_array word_to_unjumble;  //holds the jumbled word (to be unjumbled)
char_array word_being_searched;  //to read from files and compare
char_array found_words;   //container for found_words_file

char file_name[7]="AA.txt";

char temp;  //for swapping
char choice = 'n';    //exit control for do-while loop
int flag;   //to see if a word has been found - one for yes and zero for no
unsigned int i;  //sidekick

// functions

// searches if a given word exists in the dictionary
void search_for_word(char_array word_to_search)
{
	// find the corresponding dictionary file
    file_name[0] = word_to_search.word[0];
    file_name[1] = word_to_search.word[1];
    
    //open it, or atleast try to
    reqd_dict_file.open(file_name, ios::binary|ios::in);
    
    //check if the file exists and is good for i/o operations
    if (reqd_dict_file.good())  
    {
        reqd_dict_file.read((char *)&word_being_searched, sizeof(char_array));
        
        // cheap optimization to check if third letter matches
        // works because minimum word length asked for is three
        while (!reqd_dict_file.eof() && word_being_searched.word[2] <= word_to_search.word[2])
        {
			// compare full word now
            if (strcmp(word_to_search.word, word_being_searched.word) == 0)
            {
				// word has been found
                flag = 1;
                
                // check if word has already been found
                // this check is required for words with two or more occurrence of the same letter,
                // because the same permutation may be generated
                found_words_file.clear();
                found_words_file.seekg(0);
                found_words_file.read((char *)&found_words, sizeof(char_array));
                while (!found_words_file.eof())
                {					
                    if (strcmp(found_words.word, word_being_searched.word) == 0)
                    {
						// word has already already been found once
                        break;
                    }
                    found_words_file.read((char *)&found_words, sizeof(char_array));
                }
                
                if(found_words_file.eof())
                {
					// word has not been found earlier
					// so add it
                    found_words_file.clear();
                    found_words_file.seekg(0, ios::end);
                    found_words_file.write((char *)&word_being_searched, sizeof(char_array));
                    
                    // print results as and when we get them
                    cout << word_to_search.word << endl;
                }
                
            }
            reqd_dict_file.read((char *)&word_being_searched, sizeof(char_array));
        }
    }
    reqd_dict_file.close();
}

void search_power_set(char *set, int set_size)
{
	
    /*set_size of power set of a set with set_size
      n is (2**n -1)*/
    char new_word[50];
    char_array word_to_search;
    unsigned int pow_set_size = pow(2, set_size);
    int counter, j, i;
    /*Run from counter 000..0 to 111..1*/
    for(counter = 0; counter < pow_set_size; counter++)
    {
	  i = 0;
      for(j = 0; j < set_size; j++)
       {
          /* Check if jth bit in the counter is set
             If set then pront jth element from set */
          if(counter & (1<<j))
          {
            new_word[i] = set[j];
            i++;
		  }
       }
       new_word[i] = '\0';
       if (strlen(new_word) < 5)
			continue;
	   if (strlen(new_word) > 10)
			continue;	// that word won't be in the dictionary anyway
	   strcpy(word_to_search.word, new_word);
       search_for_word(word_to_search);
    }
}

int main()
{
	
	char word_to_name[50];
    
    do
    {
        flag = 0;
        
        cout << "Enter word: ";
        
        found_words_file.open("found.txt", ios::out | ios::binary | ios::in | ios::trunc);
        
        cin.getline(word_to_name, 50);
        
        for(i = 0; i < strlen(word_to_name); i++)
        {
            word_to_name[i] = toupper(word_to_name[i]);
        }
        
        cout << "Loading results, this may take some time...\n";
        
        cout << "Possible solution(s):\n";
        
        search_power_set(word_to_name, strlen(word_to_name));
        
        if (flag == 0)
        {
            cout << "(No words found)." << endl;
        } 
        
        choice = 'n';
        
        found_words_file.close();
        
    } while(choice == 'y' || choice == 'Y');
    
    return 0;
}
